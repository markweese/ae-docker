let arr1;
let arr2;

try {
  arr1 = JSON.parse(process.argv[process.argv.length-1]);
  arr2 = JSON.parse(process.argv[process.argv.length-2]);

  console.log(arr1.concat(arr2).sort((a,b) => { return a - b; }));
} catch {
  console.log('please enter two valid arrays of numbers in [x,x,x] format');
}
