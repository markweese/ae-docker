FROM node:alpine
WORKDIR /usr/app
COPY ./index.js ./
ENTRYPOINT ["node", "index.js"]
CMD [$1, $2]
